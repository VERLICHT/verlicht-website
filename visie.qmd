---
title: "Visie"
---

## 🐥VERLICHT visie🐤

Onze visie is een integrale, iteratieve, inductieve systeemmonitoring van wijken en interventies om zo bij te dragen aan een wereld waar vrijheid, geluk en gezondheid zo veel mogelijk worden geoptimaliseerd en het voor je levenskansen niet uitmaakt waar je wordt geboren.

## 🐥VERLICHT missie🐤

We willen deze visie bereiken door een monitoringsaanpak te ontwikkelen die kwalitatieve en kwantitatieve methoden, primair en secundair onderzoek, en evidence-based practice en practice-based evidence combineert. Deze monitoring is gebaseerd op een opvatting van wijken als complexe systemen en waarbij belangen van burgers op de eerste plaats staan.

## 🐥VERLICHT kernwaarden🐤

1. **Burgers voorop.** Monitoring bestaat voor burgers, niet voor organisaties. VERLICHT zet zich in om te zorgen dat burgers zo weinig mogelijk worden belast voor de monitoring. Bovendien streven we ernaar dat elke keer dat burgers wel worden belast, duidelijk is hoe zij uiteindelijk baat kunnen hebben bij de resultaten van de monitoring.
2. **Maximale transparantie.** Monitoring is zo open als mogelijk, zo gesloten als nodig. Zowel overheden als de academische gemeenschap hebben zich gecommitteerd aan openheid via bijvoorbeeld de Wet open overheid, de UNESCO Open Science aanbeveling, en de Gedragscode Wetenschappelijke Integriteit: VERLICHT betracht daarom maximale openheid.
3. **Respecteer complexiteit.** We accepteren dat de wereld complex is en versimpelen deze zo weinig mogelijk. In plaats van dat we versimpeling toepassen in dataverzameling en modellering, passen we die toe in de ontsluiting van inzichten via datavisualisaties en dashboards. We ondersteunen duiding van deze producten, zo veel mogelijk afgestemd op de belanghebbende.
4. **Epistemologische en methodologische integriteit.** We doen ons best om onszelf de hoogste epistemologische en methodologische standaarden op te leggen. Tegelijkertijd erkennen we dat dit project geen fundamenteel wetenschappelijk project is, dus balanceren we deze kernwaarde met pragmatische overwegingen op basis van de overige kernwaarden.

## 🐥VERLICHT strategieën🐤

1. Een methode uitwerken om mensen te interviewen, zodanig dat expliciete uitspraken worden verkregen die aansluiten bij de Kwalitatieve Netwerk Benadering (QNA; de [Qualitative Network Approach](https://doi.org/hwzj){.external target="_blank"}).
2. Procedures ontwikkelen om efficiënt met [`{metabefor}`](https://metabefor.opens.science){.external target="_blank"} (een R package) systematische levende reviews op te zetten.
3. Een inventaris opstellen van databases en andere bestaande databronnen, hun APIs, en de koppelingsmogelijkheden.
4. Infrastructuur ontwikkelen om de gecodeerde transcripten, de resulterende netwerken, gekoppelde datapunten, en toegevoegde metadata op te slaan en verder te verwerken.
5. Procedures en instrumenten ontwikkelen om stuurinzichten te produceren op basis van onze infrastructuur.
6. Documentatie ontwikkelen voor al het bovenstaande.

