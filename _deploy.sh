### In Plesk, execute with:
### bash _deploy.sh >> _deployment.log 2>&1

echo ----------
echo Starting at: $(date)

### Go to directory with cloned git repo
cd ~/deploy_verlicht.one

### Set the path so LaTeX can be found
PATH=$PATH:/var/www/vhosts/verlicht.one/.phpenv/shims:/opt/plesk/phpenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/var/www/vhosts/verlicht.one/.TinyTeX/bin/x86_64-linux

### Delete old 'public' directory if it exists
#rm -rf public

echo Current directory: $(pwd)
echo Current '$PATH': $PATH
echo Running Quarto

### Render the site
/usr/local/bin/quarto render

echo Done with Quarto. Deleting old directories and files...

### Delete all contents in public HTML directory
rm -rf ~/httpdocs/*.*
rm -rf ~/httpdocs/*
rm -f ~/httpdocs/.htaccess

echo Done deleting old directories and files. Copying over new website...

### Copy website
cp -RT public ~/httpdocs

### Copy .htaccess
cp .htaccess ~/httpdocs

echo Done copying over new website.

# cp -a ./public/. ~/httpdocs/
# 
# cp .htaccess ~/httpdocs/

echo Finished at: $(date)
echo ----------